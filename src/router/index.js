import Vue from 'vue';
import VueRouter from 'vue-router';

const ListReports = () => import(/* webpackChunkName: "listReports" */ '../views/ListReports.vue');
const CreateReport = () => import(/* webpackChunkName: "createReport" */ '../views/CreateReport.vue');
const ReportEditor = () => import(/* webpackChunkName: "reportEditor" */ '../views/ReportEditor.vue');

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    redirect: { name: 'list-reports' },
  },
  {
    path: '/list-reports',
    name: 'list-reports',
    component: ListReports,
  },
  {
    path: '/create-report',
    name: 'create-report',
    component: CreateReport,
  },
  {
    path: '/report-editor/:reportType',
    name: 'report-editor',
    component: ReportEditor,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
