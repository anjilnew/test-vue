const state = () => ({
  savedReports: [],
});

const mutations = {
  saveReport(store, payload) {
    // eslint-disable-next-line no-param-reassign
    store.savedReports.push(payload);
  },
};

const actions = {
  saveReport({ commit }, payload) {
    commit('saveReport', payload);
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
};
